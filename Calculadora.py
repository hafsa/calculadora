def sumar(num1, num2):
    return num1 + num2

def restar(num1, num2):
    return num1 - num2

resultado1 = sumar(1, 2)
print("El resultado de sumar 1 + 2 es", resultado1)

resultado2 = sumar(3, 4)
print("El resultado de sumar 3 + 4 es", resultado2)

resultado3 = restar(6, 5)
print("El resultado de restar 6 - 5 es", resultado3)

resultado4 = restar(8, 7)
print("El resultado de restar 8 - 7 es", resultado4)
